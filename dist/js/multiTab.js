/* multiTab.js
  This script add multi tab and window support
*/

// Creating new namespace for multiTab functions
var multiTab = {}

if (typeof (Storage) !== 'undefined') {
  // Browser support web storage
  if (log) console.info(`Browser support Web Storage: ${typeof (Storage)}`)
  multiTab.support = true
  window.addEventListener('storage', function (e) {
    if (log) console.log('Storage date changed')
    for (var key in windowElements) {
      // Checking local elements
      if (log) console.log(`Checking windows element: ${key}`)
      if(localStorage.getItem(key) !== null) {
        if(localStorage.getItem(key) !== windowElements.key) {
          if (log) console.info(`Element '${key}' changed`)
          // Run multiTab function to check if update needed
          multiTab.run(key, localStorage.getItem(key))
        }
      }
    }
  })
} else {
  // No web storage support or its disabled
  if (log) console.info(`Browser NOT support Web Storage: ${typeof (Storage)}`)
  multiTab.support = false
}

/* windowElements.set
   When adding or changing data to windowElements
   use function windowElements.set (key, value).
   This will tricker Web Storage if posible.
*/
windowElements.set = function (key, value) {
  if (log) console.log(`windowElements data set: ${key}`)
  if (log) console.log(value)
  windowElements[key] = value
  if (multiTab.support) {
    localStorage.setItem(key, value)
  }
}

// Function for running other functions in namespace
multiTab.run = function (func) {
  if (log) console.log(`Runnin multiTab function: ${func}`)
  this[func].apply(this, Array.prototype.slice.call(arguments, 1))
}

// multiTab 'cookie' function
multiTab.cookie = function (target) {
  // If target is 'false', cookie must be unmounted
  if (log) console.debug(`multiTab cookie: ${target}`)
  if (!target || target == 'false') {
    if (document.getElementById('cookie')) {
      if (log) console.log('Unmounting cookie notification')
      // Cookie exist
      document.getElementById('cookie-allow').removeEventListener('click', function () {
        if (log) console.info('User disallows cookie')
        start(true)
      })
      document.getElementById('cookie-disallow').removeEventListener('click', function () {
        if (log) console.info('User disallows cookie')
        start(false)
      })
      unmount(document.body, cookie)
      windowElements.cookie = false
    }
  }
}
