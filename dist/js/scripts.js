// When page is ready
const { unmount } = redom
const api = axios.create({
  baseURL: 'https://lauta.xyz/api/v1/',
  timeout: 10000,
  headers: { 'content-type': 'application/json' }
})

const timeEnds = () => {
  console.log('!!! hello from inside announce() !!!')
  let ran = Timeout.set(timeEnds, 10000)
}

const iSotimeEnds = () => {
  console.log('!!! hello from inside announce() !!!')
  let iso = Timeout.set(iSotimeEnds, 10000)
}

let ran = Timeout.set(timeEnds, 30000)
let iso = Timeout.set(iSotimeEnds, 1000000)

(function updateAllStatuses () {
  document.getElementById('timer').innerHTML = Math.round(Timeout.remaining(timeEnds) / 1000)
  document.getElementById('ISOtimer').innerHTML = Math.round(Timeout.remaining(iSotimeEnds) / 1000)
  // we can keep setting a new timeout like this
  // without worrying if we're creating overlapping timers
  // because a maximum of 1 may ever exist for a given key
  Timeout.set('updateAllStatuses', updateAllStatuses, 1000)
}())

start()
getBoards()

// Starting webapp
function start (allow = undefined) {
  // RE:DOM element, for asking cookie
  if(Cookies.get('token') === undefined) { // Cookie not set
    if (log) console.log('Token cookie undefined')
    if (allow === undefined) { // GDPR allow cookies
      if (log) console.log('Asking cookie access')
      localStorage.setItem('cookieAsk', true)
      // append body
      mount(document.body, cookie)
      windowElements.set('cookie', true)
      document.getElementById('cookie-allow').addEventListener('click', function () {
        if (log) console.info('User allows cookie')
        start(true)
      })
      document.getElementById('cookie-disallow').addEventListener('click', function () {
        if (log) console.info('User disallows cookie')
        start(false)
      })
    } else { // allow is defined
      if (document.getElementById('cookie')) {
        if (log) console.log('Unmounting cookie notification')
        // Cookie exist
        document.getElementById('cookie-allow').removeEventListener('click', function () {
          if (log) console.info('User disallows cookie')
          start(true)
        })
        document.getElementById('cookie-disallow').removeEventListener('click', function () {
          if (log) console.info('User disallows cookie')
          start(false)
        })
        unmount(document.body, cookie)
        windowElements.set('cookie', false)
      }
      const body = { address: '123.123.123.123' }
      api.post('/users/user?user_type=session', {
        data: JSON.stringify(body)
      })
        .then(function (response) {
          if (log) console.debug(response)
          if (response.data.token && response.data.csrfToken) {
            if(allow) { // Check do user allow cookies
              Cookies.set('token', response.data.token, { expires: 300 }) // 300 days
              Cookies.set('csrf_token', response.data.csrfToken, { expires: 300 }) // 300 days
            } else {
              Cookies.set('token', response.data.token) // Cookie dies after page is closed
              Cookies.set('csrf_token', response.data.csrfToken) // Cookie dies after page is closed
            }

          }
        })
        .catch(function (error) {
          if (log) console.error(error)
        })
    }
  } else { // Cookie already set
    console.log('Token found')
    // Check if cookie box exists, multi tab support
    if (document.getElementById('cookie')) {
      if (log) console.log('Unmounting cookie notification')
      // Cookie exist
      document.getElementById('cookie-allow').removeEventListener('click', function () {
        if (log) console.info('User disallows cookie')
        start(true)
      })
      document.getElementById('cookie-disallow').removeEventListener('click', function () {
        if (log) console.info('User disallows cookie')
        start(false)
      })
      unmount(document.body, cookie)
      windowElements.set('cookie', false)
    }
  }
}


const APIURLV1 = 'https://lauta.xyz/api/v1';

String.prototype.capitalize = function() {  // capitalize input string
  return this.charAt(0).toUpperCase() + this.slice(1);
};

function getCookie(name) {  // get cookie by name
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length === 2) return parts.pop().split(";").shift();
}

// get board listing for sidebar
function getBoards() {
  api.get('/boards')
    .then(function (response) { // Success
      if (log) console.log(response)
      if (log) console.log(response.data.boards)
      boards.update(response.data.boards[0])
    })
    .catch(function (error) { // Error
      if (log) console.error(error)
    })
    .then(function () { // Always
    })
}

// get single board information
function getBoard(short) {
  console.log("Getting board " + short + " from API...");
  const boardHeader = document.getElementById('board_header');
  const request = new XMLHttpRequest();
  request.open('GET', APIURLV1 + '/boards/board/' + short, true);
  request.onload = function () {
    const data = JSON.parse(this.response);
    const board = data.board[0];
    if (request.status >= 200 && request.status < 400) {
      const boardHeading = document.createElement('p');
      boardHeading.setAttribute('class', 'h1');
      boardHeading.textContent = board.heading.capitalize();
      const boardDesc = document.createElement('p');
      boardDesc.setAttribute('class', 'board_desc');
      boardDesc.textContent = board.desc;
      boardHeader.appendChild(boardHeading);
      boardHeader.appendChild(boardDesc);
    } else {
      console.log('error');
    }
  };
  request.send();
}

// new session user
function sessionUser() {
  if(!getCookie('token')) {
    console.log('No token found, creating new session user...');
    const url = APIURLV1 + "/users/user?user_type=session";
    fetch(url, {
      method : "POST",
      body : JSON.stringify({
        address : "123.123.123.123",
      })
    }).then(response => response.json())
      .then(function(response){
          document.cookie = "token=" + response.token;
          document.cookie = "csrf_token=" + response.csrfToken;
          console.log('New session user created.');
        }
      );
  }
}

// new registered user
function registerUser() {
  if(getCookie('token')) {
    console.log('Token found, creating new registered user...');
    const form = document.getElementById("register-form");
    if(form.elements[1].value === form.elements[2].value) {
      const url = APIURLV1 + "/users/user?user_type=permanent";
      fetch(url, {
        method: "POST",
        headers: new Headers({
          'Content-Type': 'application/json',
          'token': getCookie('token'),
        }),
        body: JSON.stringify({
          nickname: form.elements[0].value,
          password: form.elements[1].value,
        })
      }).then(response => response.json())
        .then(response => console.log('Success:', JSON.stringify(response)))
        .catch(error => console.error('Error:', error));
    } else {
      console.log('passwords don´t match');
    }
  } else {
    console.log('error: no session token found');
  }
}

// sidebar toolset button functions

toolsetAction.pageUp = function () {
  if (log) console.log('Toolset page up')
  window.scrollTo(0, 0)
}

toolsetAction.pageDown = function () {
  if (log) console.log('Toolset page down')
  window.scrollTo(0, document.body.scrollHeight)
}

toolsetAction.settings = function () {
  if (log) console.log('Toolset open settings')
  // TODO: Open settings
}

toolsetAction.pageRefresh = function () {
  if (log) console.log('Toolset page refresh')
  window.location.reload()
}

toolsetAction.hideSidebar = function () {
  if (log) console.log('Toolset hide sidebar')
  // TODO: Sidebar hiding
}

// authentication box

function toggleAuth () {
  var authenticationDiv = document.getElementById('authentication')

  if (authenticationDiv.classList.contains('authentication-open')) {
    authenticationDiv.classList.remove('authentication-open')
    document.getElementById('auth-log-in').style.display = 'inline-block'
    document.getElementById('auth-chevrons-up').style.display = 'none'
  } else {
    authenticationDiv.classList.add('authentication-open')
    document.getElementById('auth-log-in').style.display = 'none'
    document.getElementById('auth-chevrons-up').style.display = 'inline-block'
  }
  // TODO: Open settings
}

function updatePage () {
  if (log) console.log('Page update request')
}
