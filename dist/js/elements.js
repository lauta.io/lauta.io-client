/* All RE:DOM elements are listed here */

const { el , list, text, mount } = redom

// Cookie box element
const cookie = el('div.cookie#cookie', [
  el('p', 'Sivusto tarjoaa Sinulle keksiä, haluatko ottaa keksin vastaan?'),
  el('p', 'Keksiä ei käytetä markkinointiin tai käyttäytymisesi seuraamiseen, vain kokemuksesi parantamiseen.'),
  el('button.button.button-primary#cookie-allow', 'Hyväksyn'),
  el('button.button.button-white#cookie-disallow', 'Ei kiitos')
])

// Toolset row element

const toolsetPageUp = el('span#toolset-page-up', el('i', { 'data-feather': 'arrow-up' }))
const toolsetPageDown = el('span#toolset-page-down', el('i', { 'data-feather': 'arrow-down' }))
const toolsetSettings = el('span#toolset-settings', el('i', { 'data-feather': 'settings' }))
const toolsetRefresh = el('span#toolset-page-refresh', el('i', { 'data-feather': 'refresh-cw' }))
const toolsetHideSidebar = el('span#toolset-hide-sidebar', el('i', { 'data-feather': 'chevrons-left' }))

toolsetPageUp.addEventListener('click', function (event) {
  toolsetAction.pageUp()
})
toolsetPageDown.addEventListener('click', function (event) {
  toolsetAction.pageDown()
})
toolsetSettings.addEventListener('click', function (event) {
  toolsetAction.settings()
})
toolsetRefresh.addEventListener('click', function (event) {
  toolsetAction.pageRefresh()
})
toolsetHideSidebar.addEventListener('click', function (event) {
  toolsetAction.hideSidebar()
})

const toolset = el('div.toolset', [
  toolsetPageUp,
  toolsetPageDown,
  toolsetSettings,
  toolsetRefresh,
  toolsetHideSidebar
])

// logo url and element
const logoUrl = (dev) ? 'dist/svg/laama-light.svg' : '/dist/svg/laama-light.svg'
const logo = el('img.laama', { src: logoUrl })

// Menu after logo and before authentication
class Menu {
  constructor () {
    this.el = el('li',
      this.a = el('a')
    )
    this.a.addEventListener('click', function (event) {
      if (log) console.log('Menu item clicked')
      if (dev) console.debug(event)
      event.preventDefault()
    }, false)
  }
  update ({ href, text }) {
    this.a.textContent = text
    this.a.href = href
  }
}

const menu = list('ul', Menu, 'id')

// Authentication section
const authBtn = el('button.button.button-block.button-icon.auth#authentication-button', [
  el('span', 'Tunnistaudu'),
  el('i#auth-log-in', { 'data-feather': 'log-in' }),
  el('i#auth-chevrons-up', { 'data-feather': 'chevrons-up', style: 'display: none;' })
])

authBtn.addEventListener('click', function (event) {
  if (log) console.log('Authentication button pressed')
  toggleAuth()
})

class Auth {
  constructor () {
    this.el = el('form', [
      el('div.field.field-float-label', [
        this.nickname = el('input#nickname', { type: 'text', placeholder: 'Käyttäjätunnus' }),
        el('label', { for: 'nickname' }, 'Käyttäjätunus')
      ]),
      el('div.field.field-float-label', [
        this.password = el('input#password', { type: 'password', placeholder: 'Salasana' }),
        el('label', { for: 'password' }, 'Salasana')
      ]),
      el('div.field', [
        this.submit = el('button.button', { type: 'submit' },
          'Kirjaudu'
        )
      ])
    ])
    this.el.onsubmit = e => {
      e.preventDefault()

      console.log({
        email: this.nickname.value,
        pass: this.password.value
      })
    }
  }
}

const auth = new Auth()

// Small notifications

class SmallNotifications {
  constructor () {
    this.el = el('div.small-notifications#small-notifications', [
      this.scroll = el('div.notification-scroll#notification-scroll'),
      this.button = el('button.button.button-s.button-block#notification-button', 'Lisää ilmoituksia')
    ])
    this.button.onclick = e => {
      e.preventDefault()
      if (this.el.classList.contains('open')) {
        this.el.classList.remove('open')
      } else {
        this.el.classList.add('open')
      }
      notificationScroll.recalculate()
      // Firefox bug fix
      if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        if (this.scroll.hasAttribute('style')) {
          this.scroll.removeAttribute('style')
        } else {
          const newHeight = notificationScroll.getScrollElement().scrollHeight + 1
          this.scroll.style.height = `${newHeight}px`
        }
      }
    }
  }
}

class SmallNotification {
  constructor () {
    this.el = el('div.notification', [
      this.b = el('b'),
      this.span = el('span'),
      el('i.close', { 'data-feather': 'x' })
    ])
  }
  update ({ title, text }) {
    this.b.textContent = title
    this.span.textContent = text
  }
}

function smallNotificationUpdate (data = []) {
  const notificationButton = document.getElementById('notification-button')
  const notificationEl = document.getElementById('small-notifications')
  console.log(data.length)
  if (data.length === 0) {
    notificationEl.style.display = 'none'
    if (notificationEl.classList.contains('open')) {
      notificationEl.classList.remove('open')
    }
  } else if (data.length <= 2) {
    notificationEl.style.display = 'inherit'
    notificationButton.style.display = 'none'
    if (notificationEl.classList.contains('open')) {
      notificationEl.classList.remove('open')
    }
  } else {
    notificationEl.style.display = 'inherit'
    notificationButton.style.display = 'block'
  }
  smallNotification.update(data)
  feather.replace()
  notificationScroll.recalculate()
}

const smallNotifications = new SmallNotifications()
const smallNotification = list('div.padding', SmallNotification, 'id')

// List of boards
class Boards {
  constructor () {
    this.el = el('li', [
      this.a = el('a'),
      this.badge = el('span')
    ])
  }
  update ({ heading, short, nsfw }) {
    this.a.textContent = heading
    this.a.href = short
    if (nsfw) {
      this.badge.textContent = 'NSFW'
    } else {
      this.badge.style = 'display: none;'
    }
  }
}

const boards = list('ul', Boards, 'order_number')

const sidebar = el('div.sidebar', [
  toolset,
  logo,
  el('div.boards', menu),
  authBtn,
  el('div.authentication#authentication', auth),
  smallNotifications,
  el('div.boards', boards)
])

mount(document.body, sidebar)
mount(document.getElementById('notification-scroll'), smallNotification)
const notificationScroll = new SimpleBar(document.getElementById('notification-scroll'))
feather.replace()

menu.update([
  { id: 1, href: 'https://lauta.io', text: 'Lauta.io' },
  { id: 2, href: 'https://sylky.lauta.io', text: 'sylky.Lauta.io' }
])

smallNotificationUpdate([
  { id: 1, title: 'Ilmoitus :D', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 2, title: 'Tässä toinen XD', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 3, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 4, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 5, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 6, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 7, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 8, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 9, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 10, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 11, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 12, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 13, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' },
  { id: 14, title: 'Tämä ilmoitus on piilossa :3', text: 'Hahahahhahah, pitkätkin ilmoitukset onnistuu' }
])
feather.replace()

const notificatio = el('div.notification', [
  el('div.message', [
    el('div.hero', this.hero),
    el('h4', this.title)
  ])
])
